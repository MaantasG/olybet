<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;

    class AddForeignKeysToBetSelectionsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('bet_selections', function (Blueprint $table) {
                $table->foreign('bet_id',
                    'fk_bet_selections_connection_bet_idx')->references('id')->on('bet')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table('bet_selections', function (Blueprint $table) {
                $table->dropForeign('fk_bet_selections_connection_bet_idx');
            });
        }
    }

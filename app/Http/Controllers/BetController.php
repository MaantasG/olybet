<?php

    namespace App\Http\Controllers;

    use App\Http\Requests\BetRequest;
    use App\Models\BalanceTransaction;
    use App\Models\Bet;
    use App\Models\BetSelections;
    use App\Rules\CheckBalance;
    use Carbon\Carbon;
    use Exception;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Lang;

    class BetController extends Controller
    {
        protected PlayerController $playerController;

        public function __construct(Request $request)
        {
            $this->playerController = new PlayerController();
        }

        /**
         * Make bet for provided player
         *
         * @param BetRequest $request
         * @return \Illuminate\Http\JsonResponse
         */
        public function bet(BetRequest $request): \Illuminate\Http\JsonResponse
        {
            $player = $this->playerController->findOrCreate($request->player_id);

            $checkBalance = new CheckBalance($player->balance);
            if (!$checkBalance->passes("balance", $request->stake_amount)) {
                return response()->json(["errors" => $checkBalance->message()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();
            try {
                $bet = Bet::create(['stake_amount' => $request->stake_amount, 'created_at' => Carbon::now()]);
                foreach ($request->selections as $key => $selection) {
                    BetSelections::create([
                        'bet_id' => $bet->id,
                        'selection_id' => $selection['id'],
                        'odds' => $selection['odds']
                    ]);
                }

                $balance = BalanceTransaction::create([
                    'player_id' => $player->id,
                    'amount' => ($player->balance - $request->stake_amount),
                    'amount_before' => $player->balance
                ]);

                $player->update(['balance' => $balance->amount]);
                DB::commit();
                return response()->json((object) [], Response::HTTP_CREATED);
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["errors" => ["code" => 0, "message" => Lang::get('validation.0')]],
                    Response::HTTP_CREATED);
            }
        }

    }

<?php

    namespace App\Rules;

    use Illuminate\Contracts\Validation\Rule;
    use Illuminate\Support\Facades\Lang;

    class CheckBalance implements Rule
    {
        protected $balance;

        /**
         * Create a new rule instance.
         *
         * @return void
         */
        public function __construct($balance = null)
        {
            if (!$balance) {
                return [['code' => 1, 'message' => Lang::get('validation.1')]];
            }

            $this->balance = $balance;
        }

        /**
         * Determine if the validation rule passes.
         *
         * @param string $attribute
         * @param mixed $value
         * @return bool
         */
        public function passes($attribute, $value): bool
        {
            return $value <= $this->balance;
        }

        /**
         * Get the validation error message.
         *
         * @return array[]
         */
        public function message(): array
        {
            return [
                ['code' => 11, 'message' => Lang::get('validation.11')]
            ];
        }
    }

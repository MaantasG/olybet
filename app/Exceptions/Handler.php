<?php

    namespace App\Exceptions;

    use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
    use Illuminate\Http\Response;
    use Illuminate\Validation\ValidationException;
    use Throwable;

    class Handler extends ExceptionHandler
    {
        /**
         * A list of the exception types that are not reported.
         *
         * @var array
         */
        protected $dontReport = [
            //
        ];

        /**
         * A list of the inputs that are never flashed for validation exceptions.
         *
         * @var array
         */
        protected $dontFlash = [
            'current_password',
            'password',
            'password_confirmation',
        ];

        /**
         * Register the exception handling callbacks for the application.
         *
         * @return void
         */
        public function register()
        {
            $this->reportable(function (Throwable $e) {
                //
            });
        }

        /**
         * Convert a valid exception into JSON response.
         *
         * @param \Illuminate\Http\Request $request
         * @param ValidationException $exception
         * @return \Illuminate\Http\JsonResponse
         */
        protected function invalidJson($request, ValidationException $exception): \Illuminate\Http\JsonResponse
        {
            $errors = $request->toArray();

            unset($errors['player_id']);
            unset($errors['stake_amount']);
            $errors['errors'] = array();
            foreach ($exception->errors() as $key => $error) {

                $separator = explode(".", $key);

                if ($separator[0] == "selections" && isset($separator[1])) {
                    unset($errors['selections'][$separator[1]]['odds']);
                    $errors['selections'][$separator[1]]['errors'][] = $error[0];

                } else {
                    $errors["errors"] = $error;
                }
            }

            return response()->json($errors, Response::HTTP_BAD_REQUEST);
        }
    }

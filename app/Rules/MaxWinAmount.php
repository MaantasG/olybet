<?php

    namespace App\Rules;

    use Illuminate\Contracts\Validation\Rule;
    use Illuminate\Support\Facades\Lang;

    class MaxWinAmount implements Rule
    {
        protected $amount;
        protected $selections;

        /**
         * Create a new rule instance.
         *
         * @return void
         */
        public function __construct($amount, $selections = [])
        {
            if (empty($selections)) {
                return null;
            }

            $this->selections = $selections;
            $this->amount = $amount;
        }

        /**
         * Determine if the validation rule passes.
         *
         * @param string $attribute
         * @param mixed $value
         * @return bool
         */
        public function passes($attribute, $value): bool
        {
            $oddsCount = 1;
            foreach ($this->selections as $key => $item) {
                $oddsCount *= $item['odds'];
            }
            return ($oddsCount * $value) <= $this->amount;
        }

        /**
         * Get the validation error message.
         *
         * @return array|false|string
         */
        public function message()
        {
            return [
                ['code' => 9, 'message' => Lang::get('validation.9', ['max_win_amount' => $this->amount])]
            ];

        }
    }

<?php

namespace App\Http\Controllers;

use App\Models\Player;

class PlayerController extends Controller
{
    public function findOrCreate(int $id){
        if (!($player = Player::find($id)) && Player::create(['id' => $id])->save()) {
            $player = Player::find($id);
        }

        return $player;
    }
}

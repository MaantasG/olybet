<?php

    namespace App\Rules;

    use Illuminate\Contracts\Validation\Rule;
    use Illuminate\Support\Facades\Lang;

    class MinValue implements Rule
    {
        protected $min;

        /**
         * Create a new rule instance.
         *
         * @return void
         */
        public function __construct($min)
        {
            $this->min = $min;
        }

        /**
         * Determine if the validation rule passes.
         *
         * @param string $attribute
         * @param mixed $value
         * @return bool
         */
        public function passes($attribute, $value): bool
        {
            return $value >= $this->min;
        }

        /**
         * Get the validation error message.
         *
         * @return array|array[]|false|string
         */
        public function message()
        {
            return [
                [
                    'code' => 2,
                    'message' => Lang::get('validation.2', ['min' => $this->min])
                ]
            ];

        }
    }

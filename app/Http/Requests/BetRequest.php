<?php

    namespace App\Http\Requests;

    use App\Rules\MaxOddsValue;
    use App\Rules\MaxValue;
    use App\Rules\MaxWinAmount;
    use App\Rules\MinOddsValue;
    use App\Rules\MinValue;
    use Illuminate\Http\Request;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Support\Facades\Lang;

    class BetRequest extends FormRequest
    {

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules(Request $request): array
        {
            return [
                'player_id' => ['required'],
                'stake_amount' => [
                    'required',
                    new MinValue(0.3),
                    new MaxValue(10000),
                    new MaxWinAmount(20000, $request->selections),
//                    new CheckBalance(isset($this->player) ? $this->player->balance : null),
                ],
                "selections" => ['required', 'array', 'min:1', 'max:20'],
                "selections.*.id" => [
                    'required',
                    'integer',
                    'distinct'
                ],
                "selections.*.odds" => [
                    'required',
                    'numeric',
                    new MinOddsValue(1),
                    new MaxOddsValue(10000)
                ],
            ];
        }

        /**
         * Get the error messages for the defined validation rules.
         *
         * @return array
         */
        public function messages(): array
        {
            return [
                'player_id.required' => ['code' => 1, 'message' => Lang::get('validation.1')],
                'stake_amount.required' => ['code' => 1, 'message' => Lang::get('validation.1')],
                'selections.required' => ['code' => 1, 'message' => Lang::get('validation.1')],
                'selections.min' => ['code' => 4, 'message' => Lang::get('validation.4')],
                'selections.max' => ['code' => 5, 'message' => Lang::get('validation.5')],
                'selections.*.id.distinct' => ['code' => 8, 'message' => Lang::get('validation.8')],
            ];
        }
    }

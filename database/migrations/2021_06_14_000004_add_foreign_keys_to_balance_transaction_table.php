<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;

    class AddForeignKeysToBalanceTransactionTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('balance_transaction', function (Blueprint $table) {
                $table->foreign('player_id',
                    'fk_balance_transaction_connection_player_idx')->references('id')->on('player')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table('balance_transaction', function (Blueprint $table) {
                $table->dropForeign('fk_balance_transaction_connection_player_idx');
            });
        }
    }

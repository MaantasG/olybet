<?php

    namespace App\Rules;

    use Illuminate\Contracts\Validation\Rule;
    use Illuminate\Support\Facades\Lang;

    class MaxValue implements Rule
    {
        protected $max;

        /**
         * Create a new rule instance.
         *
         * @return void
         */
        public function __construct($max)
        {
            $this->max = $max;
        }

        /**
         * Determine if the validation rule passes.
         *
         * @param string $attribute
         * @param mixed $value
         * @return bool
         */
        public function passes($attribute, $value): bool
        {
            return $value <= $this->max;
        }

        /**
         * Get the validation error message.
         *
         * @return array|array[]|false|string
         */
        public function message()
        {
            return [
                ['code' => 3, 'message' => Lang::get('validation.3', ['max' => $this->max])]
            ];
        }
    }
